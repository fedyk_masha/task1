define([
    'jquery',
    'jquery-ui',
    'slick',
    'domReady!'
], function ($) {
    'use strict';

    //select
    $('#sorter').selectmenu({
        change: function( event, ui ) {
            $(this).trigger('change');
        },
        position: { my : "left-150 center+80", at: "right center" }
    });

    //slick-slider
    $('.in_logos').slick({
        arrows:false,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        variableWidth: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    centerPadding: '0',
                    slidesToShow: 2
                }
            }
        ]
    });

    //header.links cloner
    $('.panel.header > .header.links').clone().appendTo('#store\\.links');

});
