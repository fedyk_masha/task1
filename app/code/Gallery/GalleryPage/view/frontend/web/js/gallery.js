define([
    'jquery',
    'jquery/ui',
    'slick'
], function($) {
    'use strict';

    $.widget('mage.galleryWidget', {
        options:{
            arrows:false,
            centerMode:true,
            centerPadding: 0,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            variableWidth: false
        },
        _create:function(element){
            var _opt, _self;

            _self = this.element;
            _opt = this.options;

            _self.slick(_opt);
        },


    });

    return $.mage.galleryWidget;

});
